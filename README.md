# README #

TEST repository for the SLS branding application to support the development and production of css design assets for the SITS e-vision portal.

### What is this repository for? ###

* SLSP branding application based on bootstrap 3.3.7
* Supplied by Tribal to support the SITS e-vision application

### How do I get set up? ###

* [Download LTS version of node](https://nodejs.org/en/)
* run npm install
*

 
### Who do I talk to? ###

* gavin.m.thomas@bristol.ac.uk - Digital Comms