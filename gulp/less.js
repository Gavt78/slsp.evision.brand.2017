
/*
  less.js
  ===========
  compiles less from assets folder also includes sourcemaps
*/

import gulp from 'gulp';
import less from 'gulp-less';
import plugins from 'gulp-load-plugins';
const $ = plugins();


gulp.task('lessify', () => {
    return gulp.src(paths.lessFiles)
    //.pipe(plugins.plumber())
    .pipe($.sourcemaps.init())
    .pipe($.less())
    .pipe(gulp.dest(paths.destCss))
    .on('error', function (err) {
            gutil.log(err);
            this.emit('end');
        })
    .pipe($.cssmin())
    .pipe($.rename({ suffix: '.min' }))
    .pipe($.sourcemaps.write('.'))
    .pipe(gulp.dest('./build/css').on('error', gutil.log))
    .pipe(browserSync.stream({match:'**/*.css'}));

});



gulp.task('lesslint', () => {
    return gulp.src(paths.src)
        .pipe(lesshint({
configPath: './.lesshintrc'
        }))

    .pipe(lesshint.reporter())
    .pipe(lesshint.failOnError());
});
