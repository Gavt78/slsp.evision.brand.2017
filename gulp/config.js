const paths = {
  src: './app/assets/**/*.less',
  lessFiles: './app/assets/*.less',
  dest: './build',
  destCss: './build/css',
  pugtemplates: './app/views/*.pug',
  compiledhtml: './build'
};