/*
  clean.js
  ===========
  removes folders:
    - build
*/


import gulp from 'gulp'
import plugins from 'gulp-load-plugins';
import del from 'del';

gulp.task('clean', () => {
    return del(['build/css/*.*', 'build/*.html']).then(paths => {
    console.log('Deleted files and folders:\n', paths.join('\n'));
    });
});