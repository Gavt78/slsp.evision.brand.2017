/*
  watch.js
  ===========
  watches less/pug
*/

import gulp from 'gulp'
import config from './config.js'

// Watch task
gulp.task('watch', () => {
    gulp.watch(paths.src, ['lessify'], {awaitWriteFinish:true});
    gulp.watch(paths.pugtemplates, ['pug']);
    gulp.watch('./build/*.html', ['browsersync-reload']);
})