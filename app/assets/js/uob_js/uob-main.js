//  UoB main Javascript File
//
// *********************************************************************
// Version History
//
// Dev		Date		Version		Comments
// DB12863	09/03/2017	0.1		Created file	
// retm     11/04/2017  0.2     Added function to randomise background image
// retm     19/04/2017  0.3     Added function to add class to forgotten password link
// *********************************************************************
// Function list
//
// A list of functions available in the file to go here:
// E.G. moveButton - this function moves buttons into the main button container 
//
// 1. moveTTQButs - called on every page - moves TTQ buttons
// 2. setBackgroundImage - called on body_start.hts - radomises background image on login and logout pages
// 3. setForgotPassHref -  called on body_start.hts - adds a class to the forgotten password href so that it appears as a link
// 4.
//
// *********************************************************************
// Functions
// ---------------------------------------------------------------------
// Each function must include a description at the start.
// Each function must be separated from the previous function with //---------------------------------------------

//---------------------------------------------
//This function moves TTQ Buttons with the data-move-button attribute into the button container
//It's called on every page
//Function starts here---------
 
function moveTTQButs(){

//Put all TTQ butttons with attribute data-move-button into variable

var buts = $("input[data-move-button]");

//If there are buttons execute statement else do nothing

if (buts.length > 0) {

//iterate through buttons and create a new div in button container

for (i = 0; i < buts.length; i++){

// create a unique div id

var divId = "newDiv" + (i); 

//add the div

$(".sv-btn-container").prepend("<div id='"+ divId +"'></div>");

//add the button to the div and the class to the button

$('#' + divId).prepend(buts[i]).children().addClass("sv-btn sv-btn-block");

};

//remove all classes from children in the button container and add the col 2 class

$(".sv-btn-container").children().removeClass().addClass("sv-col-sm-2");

//calculate the required offset class and generate the class stem currently 6 buttons maximum

var OS = (6 -  ($(".sv-btn-container").children().length)); 
var classStem = "sv-col-sm-2 sv-col-sm-offset-";
var classOS = classStem + OS;

//add calculated class to children of the button block

$(".sv-btn-container").children().first().addClass(function(){
   
   return classOS;
   
   });

} 

//end of if statement

}
//---------------------------------------------------------
//
// *********************************************************************
//This function radomises the background image on the login page.  It's only called on the login page
//there are 12 image files on the folder the function uses a random number to select an image

function setBackgroundImage() {
	
// Set a random image with jQuery and CSS background-image.
// there are 12 images so we generate a random number between 1 and 12 
// we need to ignore images 1,4,5,6 and 8 until we add a transparent overlay to make the text readable

var imgnos = [2,3,7,9,10,11,12];
var rndimgno = imgnos[Math.floor(Math.random()*imgnos.length)];


//$('body').css({'background-image': 'url(../images/uob/uob-' + (Math.floor(Math.random() * 12) + 1) + '.jpg)'});

$('body').css({'background-image': 'url(../images/uob/uob-' + rndimgno + '.jpg)'});
	
}
//---------------------------------------------------------
//
// *********************************************************************
//this function changes the forgot password button into a link by adding the class sv-btn-link
function setForgotPassHref () {
	
$('div.uob-login-href').find("[href]").addClass('sv-btn-link');	

}





//---------------------------------------------------------
//
// *********************************************************************
// Documemt.ready section - this JS will run on every page without specifically being called.

$(function () {
    "use strict";

	//move TTQ buttons********************************************
	
	moveTTQButs();
    
	//***************************************************************

    //SCROLL
    $('a[href*=#]:not([href=#])').click(function () {
        if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });

    //-----------------------------------------------------------------------------\    
    // Top button
    var offset = 300,
        offset_opacity = 1200,
        $back_to_top = $('.cd-top');

    //hide or show the "back to top" link
    $(window).scroll(function(){
        ( $(this).scrollTop() > offset ) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
        if( $(this).scrollTop() > offset_opacity ) { 
            $back_to_top.addClass('cd-fade-out');
        }
    });
    
});
