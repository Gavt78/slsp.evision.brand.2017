import gulp from 'gulp';
import gutil from 'gulp-util';
import plugins from 'gulp-load-plugins';
import fs from 'fs';
const $ = plugins({
    rename: {
        'gulp-html-beautify': 'beautify',
        'gulp-nunjucks-render': 'nunjucksRender'
    }
});
import browserSync from 'browser-sync';
const reload = browserSync.reload;
import del from 'del';
import reporter from 'postcss-reporter';
import postcss from 'postcss';
import less_syntax from 'postcss-less';

 function getPackageJsonVersion () {
    return JSON.parse(fs.readFileSync('./package.json', 'utf8')).version;
  };

const paths = {
    src: ['./app/assets/less/**/*.less', './app/assets/*.less', './app/assets/less/uob/*.less'],
    lessFiles: './app/assets/*.less',
    allLessFiles:['./app/assets/less/**/*.less', './app/assets/*.less'],
    fonts:'./app/assets/fonts/*.*',
    images:'./app/assets/images/**/*.{png,svg,jpg}',
    js:'./app/assets/js/**/*.js',
    dest: './build/',
    destCss: './build/assets/css/',
    nunjuckstemplates: './app/views/nunjucks/**/*.njk',
    compiledhtml: 'build/'
};

const AZURE_STORAGE_ACCESS_KEY = process.env.AZURE_STORAGE_ACCESS_KEY



import deployCdn from 'gulp-deploy-azure-cdn';


gulp.task('upload-app-to-azure', ()  =>{
    return gulp.src('build/**', {
        base: 'build' // optional, the base directory in which the file is located. The relative path of file to this directory is used as the destination path
    }).pipe(deployCdn({
        containerName: 'cdn-files', // container name in blob
        serviceOptions: ['uobassets', AZURE_STORAGE_ACCESS_KEY], // custom arguments to azure.createBlobService
        containerOptions: {publicAccessLevel: "container"},
        folder: '', // path within container
        zip: false, // gzip files if they become smaller after zipping, content-encoding header will change if file is zipped
        deleteExistingBlobs: false, // true means recursively deleting anything under folder
        concurrentUploadThreads: 10, // number of concurrent uploads, choose best for your network condition
        metadata: {
            cacheControl: 'public, max-age=31530000', // cache in browser
            cacheControlHeader: 'public, max-age=31530000' // cache in azure CDN. As this data does not change, we set it to 1 year
        },
        testRun: false // test run - means no blobs will be actually deleted or uploaded, see log messages for details
    })).on('error', gutil.log);
});



// BrowserSync proxy
gulp.task('browser-sync', ['lessify', 'build:html'], () => {
     browserSync.init({
        server: {
            baseDir: "./build"
        }
    });
});


// BrowserSync reload all Browsers
gulp.task('browsersync-reload', () => {
    browserSync.reload();
});

// Build html using nunjucks compiler 
gulp.task('build:html', () => {
    return gulp.src('app/views/nunjucks/pages/**/*.njk')
        .pipe($.nunjucksRender(nunjucksOptions))
        .pipe(gulp.dest(paths.compiledhtml))
        .pipe(browserSync.stream({ match: '**/*.html' }));
});

const nunjucksOptions = {
    path: ['./app/views/nunjucks/partials'],
    watch:true,
    manageEnv:function(env){
        const data = JSON.parse(fs.readFileSync('./package.json'));
        const version = data.version
        env.addGlobal('version',version);
        console.log("Asset version number: " +  version)
             console.log(process.env)
    }
}


// Watch task
gulp.task('watch', ['browser-sync'], () => {
    gulp.watch(paths.src, ['lessify'], ['browsersync-reload'], { awaitWriteFinish: true });
    gulp.watch(paths.nunjuckstemplates, ['build:html'],  ['browsersync-reload'], { awaitWriteFinish: true });
    gulp.watch('./build/*.html', ['browsersync-reload'], { awaitWriteFinish: true });
})

// Compile all Less files , minify and produce sourcemaps
gulp.task('lessify',() => {
     const version = getPackageJsonVersion();
    return gulp.src(paths.lessFiles)
        .pipe($.sourcemaps.init())
        .pipe($.less())
        .pipe(gulp.dest(paths.destCss + `${version}`).on('error', gutil.log))
        .pipe(browserSync.stream({ match: '**/*.css' }))
        .on('error', function(err) {
            gutil.log(err);
            this.emit('end');
        })
        .pipe($.cssmin())
        .pipe($.rename({ suffix: '.min'}))
        .pipe($.sourcemaps.write('.', {includeContent: false, sourceRoot: `assets/less`}))
        .pipe(gulp.dest(paths.destCss + `${version}`).on('error', gutil.log))
        .pipe(browserSync.stream({ match: '**/*.css' }));

});


// Less linting
gulp.task('lesslint', () => {
    return gulp.src(paths.allLessFiles)
        .pipe($.lesshint({
            configPath: './.lesshintrc'
        }))
        .pipe($.lesshint.reporter())
        .pipe($.lesshint.failOnError());
});


gulp.task('copy', ()=> {
    gulp.src([paths.fonts,paths.images,paths.js,'./app/assets/less/**','./app/assets/*.less'], {base:"./app/assets"})
        .pipe(gulp.dest('./build/assets/'));
});


// Delete files in build folder
gulp.task('clean', () => {
    return del(paths.compiledhtml).then(paths => {
        console.log('Deleted files and folders:\n', paths.join('\n'));
    });
});


// Build html and css
gulp.task('build', ['lessify', 'build:html', 'copy',]);

gulp.task('default', ['watch', 'build:html', 'browser-sync', 'copy']);



// bump version number - currently not used
gulp.task('bump', () => {
  return gulp.src('./package.json')
    .pipe($.bump())
    .pipe(gulp.dest('./'));
});
